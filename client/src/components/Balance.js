import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'
import { numberWithCommas } from '../utils/format'


export const Balance = () => {
    const { transactions } = useContext(GlobalContext)
    // Return array of amounts
    const amount = transactions.map(transaction => transaction.amount)
    // Add amounts
    const total = amount.reduce((acc, item) => (acc += item), 0).toFixed(2)

    return (
        <>
            <h4>Your Balance</h4>
            <h1 id="balance">${numberWithCommas(total)}</h1>
        </>
    )
}
